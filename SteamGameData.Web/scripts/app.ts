﻿$(function () {
    $("#btn-submit").click(function () {
        $.ajax({
            type: "post",
            url: "/Home/GetWishlistData",
            data: {
                id: $("#steam-id").val()
            },
            success: function (result) {
                $("#game-list").html(result);
                $(".btn-price-data").on("click", function () {
                    getPriceData($(this));
                });
                $("#game-query").keyup(function () {
                    queryGameList();
                });
            }
        });
    });

    $("#steam-id").keypress(function (e) {
        if (e.which == 13) {
            $("#btn-submit").click();
            return false;
        }
    });

    $(".btn-price-data").on("click", function () {
        getPriceData($(this));
    });

    $("#game-query").keyup(function () {
        queryGameList();
    });

    $('[data-toggle="tooltip"]').tooltip();

    $("#btn-settings").click(function () {
        $.ajax({
            type: "post",
            url: "/Home/GetSettings",
            success: function (result) {
                var settings = $("#settings");

                settings.html(result);

                settings.find("#select-all").change(function () {
                    if ($(this).is(":checked")) {
                        $(".store-enabled").prop("checked", true);
                    } else {
                        $(".store-enabled").prop("checked", false);
                    }
                });

                settings.find("#btn-save-settings").click(function () {
                    var values = [];
                    $(".store-enabled").each(function () {
                        var me = $(this);
                        if (me.is(":checked")) {
                            values.push(me.data("id"));
                        }
                    });
                    $.ajax({
                        type: "post",
                        url: "/Home/SaveStoreSettings",
                        data: {
                            stores: values
                        },
                        success: function () {
                            $("#success-message").toggle().delay(3000).fadeOut();
                        }
                    });
                });
                settings.find("#modal-store-data").modal("show");
            }
        })
    });
});

function queryGameList() {
    var queryText = $("#game-query").val().toString();

    $(".card-container").each(function (index) {
        var game = $(this).find(".game").html();

        if (game.toLowerCase().indexOf(queryText.toLowerCase()) >= 0) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}

function getPriceData(element) {
    element.next().toggle();
    $.ajax({
        type: "post",
        url: "/Home/GetPriceData",
        data: {
            id: element.data("id")
        },
        success: function (result) {
            var modal = $("#modal-price-data");
            modal.find(".modal-title").html(element.data("name"));

            var tbody = modal.find("#table-price-data");
            tbody.html("");

            var stores = result.disabled;
            $.each(result.values[0].list, function () {
                if (stores.indexOf(this.shop.id) > -1) {
                    tbody.append(`
                        <tr>
                            <td><a href="${this.url}" target="_blank">${this.shop.name}</a></td>
                            <td>${this.price_cut}%</td>
                            <td>$${this.price_new.toFixed(2)}</td>
                            <td>$${this.price_low.toFixed(2)}</td>
                            <td>$${this.price_old.toFixed(2)}</td>
                        </tr>
                    `);
                }
            });

            $(".header").click(function () {
                sortTable($(this), tbody);
            });

            element.next().toggle();
            modal.modal("show");
        }
    });
}

function sortTable(element, tbody) {
    var dir = "asc",
        shouldSwitch,
        switching = true,
        rows,
        i, x, y,
        switchCount = 0;

    while (switching) {
        switching = false;
        rows = tbody.find("tr");

        for (i = 0; i < rows.length - 1; i++) {
            shouldSwitch = false;

            x = $(rows[i]).find("td")[$(element).index()];
            y = $(rows[i + 1]).find("td")[$(element).index()];

            if (dir == "asc") {
                if (x.innerText.toLowerCase() > y.innerText.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerText.toLowerCase() < y.innerText.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchCount++;
        } else {
            if (switchCount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

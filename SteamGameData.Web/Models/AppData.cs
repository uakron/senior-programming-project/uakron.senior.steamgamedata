﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace SteamGameData.Web.Models
{
    public class AppData
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("capsule")]
        public string Capsule { get; set; }

        [JsonPropertyName("review_score")]
        public int ReviewScore { get; set; }

        [JsonPropertyName("review_desc")]
        public string ReviewDesc { get; set; }

        [JsonPropertyName("reviews_total")]
        public string ReviewsTotal { get; set; }

        [JsonPropertyName("reviews_percent")]
        public int ReviewsPercent { get; set; }

        [JsonPropertyName("release_date")]
        public object ReleaseDate { get; set; }

        [JsonPropertyName("release_string")]
        public string ReleaseString { get; set; }

        [JsonPropertyName("platform_icons")]
        public string PlatformIcons { get; set; }

        [JsonPropertyName("subs")]
        public List<Sub> Subs { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("screenshots")]
        public List<string> Screenshots { get; set; }

        [JsonPropertyName("review_css")]
        public string ReviewCss { get; set; }

        [JsonPropertyName("priority")]
        public int Priority { get; set; }

        [JsonPropertyName("added")]
        public int Added { get; set; }

        [JsonPropertyName("background")]
        public string Background { get; set; }

        [JsonPropertyName("rank")]
        public int Rank { get; set; }

        [JsonPropertyName("tags")]
        public List<string> Tags { get; set; }

        [JsonPropertyName("is_free_game")]
        public bool IsFreeGame { get; set; }

        [JsonPropertyName("win")]
        public int Win { get; set; }
    }
}

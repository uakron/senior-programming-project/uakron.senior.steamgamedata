﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace SteamGameData.Web.Models
{
    public static class LowestPriceData
    {
        public class Meta
        {
            [JsonPropertyName("currency")]
            public string Currency { get; set; }
        }

        public class Data
        {
            [JsonPropertyName("shop")]
            public string Shop { get; set; }

            [JsonPropertyName("price")]
            public double Price { get; set; }
        }

        public class Root
        {
            [JsonPropertyName(".meta")]
            public Meta Meta { get; set; }

            [JsonPropertyName("data")]
            public Dictionary<string, List<Data>> Data { get; set; }
        }
    }
}

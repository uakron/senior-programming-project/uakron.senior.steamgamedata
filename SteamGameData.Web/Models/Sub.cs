﻿using System.Text.Json.Serialization;

namespace SteamGameData.Web.Models
{
    public class Sub
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("discount_block")]
        public string DiscountBlock { get; set; }

        [JsonPropertyName("discount_pct")]
        public int DiscountPct { get; set; }

        [JsonPropertyName("price")]
        public int Price { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace SteamGameData.Web.Models
{
    public static class PriceData
    {
        public class Meta
        {
            [JsonPropertyName("currency")]
            public string Currency { get; set; }
        }

        public class Shop
        {
            [JsonPropertyName("id")]
            public string Id { get; set; }

            [JsonPropertyName("name")]
            public string Name { get; set; }
        }

        public class List
        {
            [JsonPropertyName("price_new")]
            public double PriceNew { get; set; }

            [JsonPropertyName("price_old")]
            public double PriceOld { get; set; }

            [JsonPropertyName("price_low")]
            public double? PriceLow { get; set; }

            [JsonPropertyName("price_cut")]
            public int PriceCut { get; set; }

            [JsonPropertyName("url")]
            public string Url { get; set; }

            [JsonPropertyName("shop")]
            public Shop Shop { get; set; }

            [JsonPropertyName("drm")]
            public List<string> Drm { get; set; }
        }

        public class Urls
        {
            [JsonPropertyName("game")]
            public string Game { get; set; }
        }

        public class StoreData
        {
            [JsonPropertyName("list")]
            public List<List> List { get; set; }

            [JsonPropertyName("urls")]
            public Urls Urls { get; set; }
        }

        public class Root
        {
            [JsonPropertyName(".meta")]
            public Meta Meta { get; set; }

            [JsonPropertyName("data")]
            public Dictionary<string, StoreData> Data { get; set; }
        }
    }
}

﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace SteamGameData.Web.Models
{
    public static class Store
    {
        public class Meta
        {
            [JsonPropertyName("region")]
            public string Region { get; set; }

            [JsonPropertyName("country")]
            public string Country { get; set; }
        }

        public class Data
        {
            [JsonPropertyName("id")]
            public string Id { get; set; }

            [JsonPropertyName("title")]
            public string Title { get; set; }

            [JsonPropertyName("color")]
            public string Color { get; set; }
        }

        public class Root
        {
            [JsonPropertyName(".meta")]
            public Meta Meta { get; set; }

            [JsonPropertyName("data")]
            public List<Data> Data { get; set; }
        }
    }
}

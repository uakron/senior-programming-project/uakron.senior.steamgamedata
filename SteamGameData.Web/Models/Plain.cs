﻿using System.Text.Json.Serialization;

namespace SteamGameData.Web.Models
{
    public static class Plain
    {
        public class Meta
        {
            [JsonPropertyName("match")]
            public string Match { get; set; }

            [JsonPropertyName("active")]
            public bool Active { get; set; }
        }

        public class Data
        {
            [JsonPropertyName("plain")]
            public string Plain { get; set; }
        }

        public class Root
        {
            [JsonPropertyName(".meta")]
            public Meta Meta { get; set; }

            [JsonPropertyName("data")]
            public Data Data { get; set; }
        }
    }
}

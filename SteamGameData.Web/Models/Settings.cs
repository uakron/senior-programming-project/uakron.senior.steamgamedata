﻿namespace SteamGameData.Web.Models
{
    public class Settings
    {
        // API key from IsThereAnyDeal.com
        public string ITAD_Key { get; set; }
    }
}

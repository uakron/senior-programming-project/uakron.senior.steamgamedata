﻿using Microsoft.AspNetCore.Http;
using System.Text.Json;

namespace SteamGameData.Web.Models
{
    public static class SessionExtensions
    {
        public static T GetComplexData<T>(this ISession session, string key)
        {
            var data = session.GetString(key);

            if (data == null)
                return default;

            return JsonSerializer.Deserialize<T>(data);
        }

        public static void SetComplexData(this ISession session, string key, object value)
        {
            session.SetString(key, JsonSerializer.Serialize(value));
        }
    }
}

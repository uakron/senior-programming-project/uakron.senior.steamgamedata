﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SteamGameData.Web.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace SteamGameData.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IOptions<Settings> _settings;
        private readonly HttpClient _client;

        public HomeController(IOptions<Settings> settings)
        {
            _settings = settings;
            _client = new HttpClient();
        }

        public IActionResult Index()
        {
            var appdata = HttpContext.Session.GetComplexData<Dictionary<string, AppData>>("appdata");

            return View(appdata);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> GetSettings()
        {
            const string url = "https://api.isthereanydeal.com/v02/web/stores/?region=us&country=US";

            using var response = await _client.GetAsync(url);
            using var content = response.Content;

            var stores = JsonSerializer.Deserialize<Store.Root>(await content.ReadAsStringAsync());

            return PartialView("Settings", new Tuple<Store.Root, List<string>>(stores, GetEnabledStores()));
        }

        public IActionResult SaveStoreSettings(List<string> stores)
        {
            var options = new CookieOptions()
            {
                Expires = DateTime.Now.AddYears(1)
            };

            Response.Cookies.Append("stores", JsonSerializer.Serialize(stores), options);

            return Json(true);
        }

        public async Task<IActionResult> GetWishlistData(string id)
        {
            var url = $"https://store.steampowered.com/wishlist/id/{id}/wishlistdata/";

            using var response = await _client.GetAsync(url);
            using var content = response.Content;

            var appdata = JsonSerializer.Deserialize<Dictionary<string, AppData>>(await content.ReadAsStringAsync());

            HttpContext.Session.SetComplexData("appdata", appdata);

            return PartialView("GameList", appdata);
        }

        public async Task<IActionResult> GetPriceData(int id)
        {
            var plainUrl = $"https://api.isthereanydeal.com/v02/game/plain/?key={_settings.Value.ITAD_Key}&shop=steam&game_id=app%2F{id}";

            using var plainResponse = await _client.GetAsync(plainUrl);
            using var plainContent = plainResponse.Content;

            var plain = JsonSerializer.Deserialize<Plain.Root>(await plainContent.ReadAsStringAsync());

            var priceUrl = $"https://api.isthereanydeal.com/v01/game/prices/?key={_settings.Value.ITAD_Key}&plains={plain.Data.Plain}&region=us&country=US";
            var lowestUrl = $"https://api.isthereanydeal.com/v01/game/storelow/?key={_settings.Value.ITAD_Key}&plains={plain.Data.Plain}&region=us&country=US";

            using var priceResponse = await _client.GetAsync(priceUrl);
            using var lowestResponse = await _client.GetAsync(lowestUrl);

            using var priceContent = priceResponse.Content;
            using var lowestContent = lowestResponse.Content;

            var priceData = JsonSerializer.Deserialize<PriceData.Root>(await priceContent.ReadAsStringAsync());
            var lowestData = JsonSerializer.Deserialize<LowestPriceData.Root>(await lowestContent.ReadAsStringAsync());

            foreach (var store in priceData.Data.Values.SelectMany(a => a.List))
                store.PriceLow = lowestData.Data.Values.SelectMany(a => a).FirstOrDefault(b => b.Shop == store.Shop.Id)?.Price;

            return Json(new { values = priceData.Data.Values, disabled = GetEnabledStores() });
        }

        private List<string> GetEnabledStores()
        {
            var cookie = Request.Cookies["stores"];
            var enabled = new List<string>();

            if (cookie != null)
                enabled = JsonSerializer.Deserialize<List<string>>(cookie);

            return enabled;
        }
    }
}
